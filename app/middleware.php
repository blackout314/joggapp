<?php
// Application middleware

use Slim\Middleware\TokenAuthentication;

$authenticator = function($request, TokenAuthentication $tokenAuth){
  $token = $tokenAuth->findToken($request);
  $auth = new \App\Auth();
  $auth->getUserByToken($token);
};

$app->add(new TokenAuthentication([
  'path' => '/restrict',
  'secure' => false,
  'authenticator' => $authenticator
]));
